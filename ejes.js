document.addEventListener("DOMContentLoaded", function() {
    var imagen = document.querySelector("img");
    
    window.addEventListener('deviceorientation', function(e) {
        a = Math.floor(e.alpha);
        b = Math.floor(e.beta);
        g = Math.floor(e.gamma);
        var transform = 'rotateZ(' + Math.round(e.alpha) + 'deg) rotateX(' + Math.round(e.beta -90) + 'deg) rotateY(' + Math.round(e.gamma) + 'deg)';
        imagen.style.transform = transform;   
    });
})